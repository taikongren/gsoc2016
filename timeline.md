# GSoC 2016 Full Program Timeline

|Event|Date|Information|
|-----|----|-----------|
|Organization Applications Open|February 8, 2016|Open source organizations that would like to participate as a mentor organization in this year?s program can apply.|
|Organization Application Deadline|February 19, 2016|All organizations wishing to be a part of GSoC 2016 must submit their application by 19:00 UTC.|
|Organizations Announced|February 29, 2016|Interested students can now begin discussing project ideas with accepted mentor organizations.|
|Student Application Period|March 14 - 25, 2016|Students can register and submit their applications to mentor organizations. All proposals must be submitted by 19:00 UTC on March 25th.|
|Student Projects Announced|April 22, 2016|Accepted students are paired with a mentor and start planning their projects and milestones.|
|Community Bonding|April 22, 2016 - May 22, 2016|Students spend a month learning more about their organization?s community.|
|Students Work on their Projects|May 23, 2016 - June 27, 2016|Students begin coding for their Google Summer of Code 2016 projects. This is the first half of the coding period.|
|Midterm Evaluations|June 20 - 27, 2016|Mentors and students submit their evaluations of one another. These evaluations are a required step of the program.|
|Students Continue Coding|June 27, 2016 - August 15, 2016||
|Students Submit Code and Evaluations|August 15 - 23, 2016|Students clean their code, write tests, improve documentation, and submit their code samples. Students also complete their final evaluations of mentors.|
|Mentors Submit Final Evaluations|August 23 - 29, 2016|Mentors review student code samples and determine if the students have successfully completed their Google Summer of Code 2016 project.|
|Results Announced|August 30, 2016|Students are notified of the pass/fail status of their Google Summer of Code 2016 projects.|
